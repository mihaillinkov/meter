FROM java:8

COPY target/meter-0.0.1.jar /app/
COPY wait-for-it.sh /app/

WORKDIR /app
USER root

RUN chmod 777 wait-for-it.sh

CMD java -jar -Dspring.profiles.active=prod meter-0.0.1.jar
