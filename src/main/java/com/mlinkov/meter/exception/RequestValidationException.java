package com.mlinkov.meter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequestValidationException extends RuntimeException {
    public RequestValidationException() {
        super();
    }

    public RequestValidationException(String message) {
        super(message);
    }
}
