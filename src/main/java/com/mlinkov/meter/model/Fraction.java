package com.mlinkov.meter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fraction {
    private String profile;
    private Map<String, BigDecimal> monthRatioMap;
}
