package com.mlinkov.meter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeterReadingProcessingResult {
    private String meterId;
    private ProcessingStatus status;
    private String message;
}
