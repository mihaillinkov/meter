package com.mlinkov.meter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeterReading {
    private String id;
    private String profile;
    private Map<String, BigDecimal> monthValueMap;
}
