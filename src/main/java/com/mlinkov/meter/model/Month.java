package com.mlinkov.meter.model;

public enum Month {
    JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC;

    public static Month getByIndex(int index) {
        return values()[index];
    }
}
