package com.mlinkov.meter.filter;

import com.mlinkov.meter.config.IntegrationConfig;
import org.springframework.integration.file.filters.FileListFilter;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HandledFileFilter implements FileListFilter<File> {
    @Override
    public List<File> filterFiles(File[] files) {
        Stream<File> stream = Stream.of(files);

        return stream
                .filter(file -> !file.getName().endsWith(IntegrationConfig.LOG_FILE_EXTENSION))
                .filter(file -> !alreadyProcessedWithError(Stream.of(files), file))
                .collect(Collectors.toList());
    }

    private boolean alreadyProcessedWithError(Stream<File> stream, File checkingFile) {
        return stream
                .anyMatch(file -> file.getName().equals(checkingFile.getName() + IntegrationConfig.LOG_FILE_EXTENSION));
    }
}
