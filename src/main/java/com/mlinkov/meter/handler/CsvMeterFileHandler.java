package com.mlinkov.meter.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mlinkov.meter.config.IntegrationConfig;
import com.mlinkov.meter.mapper.CsvMapper;
import com.mlinkov.meter.model.MeterReading;
import com.mlinkov.meter.model.MeterReadingProcessingResult;
import com.mlinkov.meter.model.ProcessingStatus;
import com.mlinkov.meter.service.MeterService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public class CsvMeterFileHandler implements MessageHandler {
    private final static Logger logger = LoggerFactory.getLogger(CsvMeterFileHandler.class);

    private CsvMapper<MeterReading> mapper;
    private MeterService meterService;
    private ObjectMapper jsonMapper;

    public CsvMeterFileHandler(CsvMapper<MeterReading> mapper, MeterService service, ObjectMapper jsonMapper) {
        this.mapper = mapper;
        this.meterService = service;
        this.jsonMapper = jsonMapper;
    }

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        File file = (File)message.getPayload();
        String fileName = file.getName();
        try {
            InputStream inputStream = FileUtils.openInputStream(file);
            List<MeterReading> meters = mapper.fromInputStream(inputStream);
            List<MeterReadingProcessingResult> results = meterService.save(meters);
            boolean hasErrors = results
                    .stream()
                    .anyMatch((result) -> result.getStatus() == ProcessingStatus.ERROR);
            if (hasErrors) {
                writeLogFile(jsonMapper.writeValueAsString(results), file);
                logger.info("File {} processed with errors", fileName);
            } else {
                file.delete();
                logger.info("File {} successfully processed", fileName);
            }
        } catch (Exception e) {
            writeLogFile("Can't handle " + fileName, file);
            logger.error("Error while handling file {}", fileName, e);
            throw new MessagingException(e.getMessage(), e);
        }
    }

    private void writeLogFile(String content, File file) {
        try {
            FileUtils.writeStringToFile(new File(file.getAbsolutePath() + IntegrationConfig.LOG_FILE_EXTENSION), content, "utf8");
        } catch (Exception e) {
            logger.error("Can't write log for file {}", file.getName(), e);
        }
    }
}
