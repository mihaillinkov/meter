package com.mlinkov.meter.config;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Configuration
public class CassandraConfig extends AbstractCassandraConfiguration {
    @Value("${spring.data.cassandra.keyspace-name}")
    private String keySpace;
    @Value("${spring.data.cassandra.contact-points}")
    private String contactPoints;
    @Value("classpath:db_fraction.cql")
    private Resource fractionScript;
    @Value("classpath:db_meter.cql")
    private Resource meterScript;

    @Override
    protected String getKeyspaceName() {
        return keySpace;
    }

    @Override
    protected String getContactPoints() {
        return contactPoints;
    }

    @Override
    protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
        return Arrays.asList(createKeyspaceSpecification());
    }

    private CreateKeyspaceSpecification createKeyspaceSpecification() {
        return CreateKeyspaceSpecification
                .createKeyspace("meter")
                .ifNotExists()
                .withSimpleReplication();
    }

    @Override
    public SchemaAction getSchemaAction() {
        return SchemaAction.CREATE_IF_NOT_EXISTS;
    }

    @Override
    protected List<String> getStartupScripts() {
        try {
            return Arrays.asList(
                    IOUtils.toString(fractionScript.getInputStream(), "utf8"),
                    IOUtils.toString(meterScript.getInputStream(), "utf8")
            );
        } catch (IOException e) {
            throw new RuntimeException("Error while reading init script");
        }
    }
}
