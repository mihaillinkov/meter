package com.mlinkov.meter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mlinkov.meter.filter.HandledFileFilter;
import com.mlinkov.meter.handler.CsvMeterFileHandler;
import com.mlinkov.meter.mapper.CsvMapper;
import com.mlinkov.meter.model.MeterReading;
import com.mlinkov.meter.service.MeterService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.RegexPatternFileListFilter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.io.File;
import java.util.Arrays;

@EnableIntegration
@Configuration
public class IntegrationConfig {
    public final static String LOG_FILE_EXTENSION = ".log";

    @Value("${meter.file.path}")
    private String path;

    @Bean
    public MessageChannel fileInputChannel() {
        return new DirectChannel();
    }

    @Bean
    @InboundChannelAdapter(channel = "fileInputChannel", poller = @Poller())
    public MessageSource<File> fileSource() {
        FileReadingMessageSource source = new FileReadingMessageSource();
        source.setDirectory(new File(path));
        source.setFilter(new CompositeFileListFilter<>(
                Arrays.asList(new RegexPatternFileListFilter("[^\\\\s]+(\\.csv)$"), new HandledFileFilter()))
        );
        source.setWatchEvents(FileReadingMessageSource.WatchEventType.CREATE);
        return source;
    }

    @Bean
    @ServiceActivator(inputChannel = "fileInputChannel")
    public MessageHandler handler(CsvMapper<MeterReading> meterCsvMapper, MeterService service, ObjectMapper jsonMapper) {
        return new CsvMeterFileHandler(meterCsvMapper, service, jsonMapper);
    }
}
