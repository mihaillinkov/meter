package com.mlinkov.meter.config;

import com.mlinkov.meter.converter.CsvListConverter;
import com.mlinkov.meter.mapper.CsvMapper;
import com.mlinkov.meter.mapper.FractionCsvMapper;
import com.mlinkov.meter.mapper.MeterCsvMapper;
import com.mlinkov.meter.model.Fraction;
import com.mlinkov.meter.model.MeterReading;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;

@Configuration
public class ConverterConfig {

    private final static MediaType CSV_MEDIA_TYPE = new MediaType("text", "csv");

    @Bean
    public CsvMapper<MeterReading> meterCsvMapper() {
        return new MeterCsvMapper();
    }

    @Bean
    public CsvMapper<Fraction> fractionCsvMapper() {
        return new FractionCsvMapper();
    }

    @Bean
    public HttpMessageConverter fractionConverter() {
        return new CsvListConverter<>(CSV_MEDIA_TYPE, Fraction.class, fractionCsvMapper());
    }

    @Bean
    public HttpMessageConverter meterReadingConverter() {
        return new CsvListConverter<>(CSV_MEDIA_TYPE, MeterReading.class, meterCsvMapper());
    }
}
