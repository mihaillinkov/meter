package com.mlinkov.meter.mapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Stream;

public abstract class CsvMapper<T> {
    public List<T> fromInputStream(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        Stream<String[]> stream = reader.lines()
                .map(String::trim)
                .filter(item -> !item.isEmpty())
                .map(line -> line.split(","));

        return createFromStream(stream);
    }

    abstract protected List<T> createFromStream(Stream<String[]> stream);
}
