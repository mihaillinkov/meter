package com.mlinkov.meter.mapper;

import com.mlinkov.meter.model.Fraction;
import com.mlinkov.meter.model.Month;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FractionCsvMapper extends CsvMapper<Fraction> {
    @Override
    protected List<Fraction> createFromStream(Stream<String[]> stream) {
        return stream.collect(Collectors.groupingBy(item -> item[1]))
                .entrySet()
                .stream()
                .map(this::createFraction)
                .collect(Collectors.toList());
    }

    private Fraction createFraction(Map.Entry<String, List<String[]>> entry) {
        return new Fraction(entry.getKey(), createMonthRatioMap(entry.getValue()));
    }

    private Map<String, BigDecimal> createMonthRatioMap(List<String[]> rows) {
        return rows
                .stream()
                .collect(Collectors.toMap(row -> Month.valueOf(row[0]).name(), row -> new BigDecimal(row[2])));
    }
}
