package com.mlinkov.meter.mapper;

import com.mlinkov.meter.model.MeterReading;
import com.mlinkov.meter.model.Month;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MeterCsvMapper extends CsvMapper<MeterReading> {
    @Override
    protected List<MeterReading> createFromStream(Stream<String[]> stream) {
        return stream
                .collect(Collectors.groupingBy(item -> item[0]))
                .entrySet()
                .stream()
                .map(this::createMeterReading)
                .collect(Collectors.toList());
    }

    private MeterReading createMeterReading(Map.Entry<String, List<String[]>> entry) {
        String[] value = entry.getValue().get(0);
        return new MeterReading(entry.getKey(), value[1], createMonthValueMap(entry.getValue()));
    }

    private Map<String, BigDecimal> createMonthValueMap(List<String[]> rows) {
        return rows
                .stream()
                .collect(Collectors.toMap(row -> Month.valueOf(row[2]).name(), row -> new BigDecimal(row[3])));
    }
}
