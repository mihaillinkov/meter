package com.mlinkov.meter.service;

import com.mlinkov.meter.exception.EntityNotFoundException;
import com.mlinkov.meter.exception.RequestValidationException;
import com.mlinkov.meter.model.Fraction;
import com.mlinkov.meter.repository.FractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class FractionService {
    @Autowired
    private FractionRepository repository;

    public List<Fraction> save(List<Fraction> fractions) {
        return repository.saveAll(fractions);
    }

    public Fraction findByProfile(String name) {
        return repository.findByProfile(name)
                .orElseThrow(() -> new EntityNotFoundException("Fraction for Profile " + name + " not found"));
    }

    public void validate(List<Fraction> fractions) {
        Optional<Fraction> errorFraction = fractions
                .stream()
                .filter(fraction -> sumRatio(fraction).compareTo(BigDecimal.ONE) != 0)
                .findFirst();

        errorFraction
                .ifPresent(fraction -> {throw new RequestValidationException("Ratio for Profile " + fraction.getProfile() + " is not equal to 1");
        });
    }

    private BigDecimal sumRatio(Fraction fraction) {
        return fraction.getMonthRatioMap()
                .values()
                .stream()
                .reduce(BigDecimal::add)
                .get();
    }
}
