package com.mlinkov.meter.service;

import com.mlinkov.meter.exception.EntityNotFoundException;
import com.mlinkov.meter.model.Fraction;
import com.mlinkov.meter.model.MeterReading;
import com.mlinkov.meter.model.MeterReadingProcessingResult;
import com.mlinkov.meter.model.Month;
import com.mlinkov.meter.model.ProcessingStatus;
import com.mlinkov.meter.repository.FractionRepository;
import com.mlinkov.meter.repository.MeterRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Setter
public class MeterService {
    protected final static String FRACTION_NOT_EXIST_MESSAGE = "Fraction for Profile %s does not exist";
    protected final static String METER_READING_VALUE_ERROR_MESSAGE = "Meter reading for Month %s less then previous";
    protected final static String METER_READING_TOLERANCE_ERROR_MESSAGE = "Meter reading for Month %s exceeds tolerance";

    private final static double TOLERANCE = 0.25;

    @Autowired
    private FractionRepository fractionRepository;
    @Autowired
    private MeterRepository meterRepository;

    public BigDecimal getMonthConsumption(String meterId, Month month) {
        MeterReading reading = this.findByMeter(meterId);
        Map<String, BigDecimal> monthValueMap = reading.getMonthValueMap();
        BigDecimal meterValue = monthValueMap.get(month.name());
        return month == Month.JAN
                ?   meterValue
                :   meterValue.subtract(monthValueMap.get(Month.getByIndex(month.ordinal() - 1).name()));
    }

    public MeterReading findByMeter(String meterId) {
        return meterRepository.findById(meterId)
                .orElseThrow(() -> new EntityNotFoundException("Meter " + meterId + " not found"));
    }

    public List<MeterReadingProcessingResult> save(List<MeterReading> meters) {
        return meters
                .stream()
                .map(this::save)
                .collect(Collectors.toList());
    }

    public MeterReadingProcessingResult save(MeterReading meter) {
        Optional<String> error = validateAndGetError(meter);
        String meterId = meter.getId();
        if (!error.isPresent()) {
            meterRepository.save(meter);
            return new MeterReadingProcessingResult(meterId, ProcessingStatus.OK, null);
        } else {
            return new MeterReadingProcessingResult(meterId, ProcessingStatus.ERROR, error.get());
        }
    }

    private Optional<String> validateAndGetError(MeterReading meter) {
        String profile = meter.getProfile();
        BigDecimal sum = meter.getMonthValueMap().get(Month.DEC.name());

        Optional<Fraction> fractionOptional = fractionRepository.findByProfile(profile);
        if (!fractionOptional.isPresent()) {
            return Optional.of(String.format(FRACTION_NOT_EXIST_MESSAGE, profile));
        }

        Fraction fraction = fractionOptional.get();

        List<BigDecimal> list = meter.getMonthValueMap()
                .entrySet()
                .stream()
                .sorted((entry1, entry2) -> Month.valueOf(entry1.getKey()).compareTo(Month.valueOf(entry2.getKey())))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        for (int i = 0; i < list.size(); i++) {
            String monthName = Month.getByIndex(i).name();
            BigDecimal currentValue = list.get(i);
            BigDecimal monthConsumption = (i == 0)
                    ?   currentValue
                    :   currentValue.subtract(list.get(i - 1));
            BigDecimal monthAverageConsumption = fraction.getMonthRatioMap().get(monthName).multiply(sum);

            if (monthConsumption.compareTo(BigDecimal.ZERO) < 0) {
                return Optional.of(String.format(METER_READING_VALUE_ERROR_MESSAGE, monthName));
            }
            if ((monthConsumption.compareTo(monthAverageConsumption.multiply(BigDecimal.valueOf(1 - TOLERANCE))) < 0)
                    || (monthConsumption.compareTo(monthAverageConsumption.multiply(BigDecimal.valueOf(1 + TOLERANCE))) > 0)) {
                return Optional.of(String.format(METER_READING_TOLERANCE_ERROR_MESSAGE, monthName));
            }
        }
        return Optional.ofNullable(null);
    }
}
