package com.mlinkov.meter.api;

import com.mlinkov.meter.model.Fraction;
import com.mlinkov.meter.service.FractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FractionController {
    @Autowired
    private FractionService fractionService;

    @GetMapping("/fraction/{profile}")
    public Fraction findFraction(@PathVariable String profile) {
        return fractionService.findByProfile(profile);
    }

    @PostMapping(value = "/fraction", consumes = {"text/csv"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void saveFractions(@RequestBody List<Fraction> fractions) {
        fractionService.validate(fractions);
        fractionService.save(fractions);
    }
}
