package com.mlinkov.meter.api;

import com.mlinkov.meter.exception.EntityNotFoundException;
import com.mlinkov.meter.exception.RequestValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerController {
    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, String> entityNotFoundException(Exception e) {
        return handleErrorAndGetResult(e);
    }

    @ExceptionHandler(value = RequestValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> requestValidationException(Exception e) {
        return handleErrorAndGetResult(e);
    }

    private Map<String, String> handleErrorAndGetResult(Exception e) {
        logger.error(null, e);
        return Collections.singletonMap("message", e.getMessage());
    }
}
