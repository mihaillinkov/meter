package com.mlinkov.meter.api;

import com.mlinkov.meter.model.MeterReadingProcessingResult;
import com.mlinkov.meter.model.Month;
import com.mlinkov.meter.model.ProcessingStatus;
import com.mlinkov.meter.model.MeterReading;
import com.mlinkov.meter.service.MeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

@RestController
public class MeterController {
    @Autowired
    private MeterService meterService;

    @GetMapping("/meter/{meterId}")
    public MeterReading findMeter(@PathVariable String meterId) {
        return meterService.findByMeter(meterId);
    }

    @GetMapping("/meter/{meterId}/{month}")
    public BigDecimal getMeterConsumptionByMonth(@PathVariable String meterId,
                                                 @PathVariable Month month) {
        return meterService.getMonthConsumption(meterId, month);
    }

    @PostMapping(value = "/meter", consumes = {"text/csv"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity saveMeters(@RequestBody List<MeterReading> meters) {
        List<MeterReadingProcessingResult> results = meterService.save(meters);

        Stream<MeterReadingProcessingResult> stream = results.stream();
        if (stream.allMatch(result -> result.getStatus() == ProcessingStatus.OK)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(results, HttpStatus.MULTI_STATUS);
        }
    }
}
