package com.mlinkov.meter.repository;

import com.mlinkov.meter.model.Fraction;
import org.springframework.data.cassandra.repository.MapIdCassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FractionRepository extends MapIdCassandraRepository<Fraction> {
    Optional<Fraction> findByProfile(String name);
}
