package com.mlinkov.meter.repository;

import com.mlinkov.meter.model.MeterReading;
import org.springframework.data.cassandra.repository.MapIdCassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MeterRepository extends MapIdCassandraRepository<MeterReading> {
    Optional<MeterReading> findById(String meterId);
}
