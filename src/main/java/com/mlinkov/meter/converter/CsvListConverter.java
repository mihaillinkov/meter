package com.mlinkov.meter.converter;

import com.mlinkov.meter.mapper.CsvMapper;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class CsvListConverter<T> extends AbstractGenericHttpMessageConverter<List<T>> {
    private Class<T> itemClass;
    private CsvMapper<T> mapper;

    public CsvListConverter(MediaType supportedMediaType, Class<T> itemClass, CsvMapper<T> mapper) {
        super(supportedMediaType);
        this.itemClass = itemClass;
        this.mapper = mapper;
    }

    @Override
    protected void writeInternal(List<T> testModels, Type type, HttpOutputMessage httpOutputMessage)
            throws IOException, HttpMessageNotWritableException {
        //do nothing
    }

    @Override
    protected List<T> readInternal(Class<? extends List<T>> aClass, HttpInputMessage httpInputMessage)
            throws IOException, HttpMessageNotReadableException {
        return null;
    }

    @Override
    public List<T> read(Type type, Class<?> aClass, HttpInputMessage httpInputMessage)
            throws IOException, HttpMessageNotReadableException {
        return mapper.fromInputStream(httpInputMessage.getBody());
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        return hasSameElementClass(type) && super.canRead(type, contextClass, mediaType);
    }

    private boolean hasSameElementClass(Type type) {
        try {
            return itemClass == ((ParameterizedTypeImpl) type).getActualTypeArguments()[0];
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return false;
    }
}
