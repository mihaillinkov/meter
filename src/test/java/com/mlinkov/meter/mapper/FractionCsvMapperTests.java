package com.mlinkov.meter.mapper;

import com.mlinkov.meter.model.Fraction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
public class FractionCsvMapperTests {
    @Value("classpath:fraction.csv")
    private Resource fractionCsv;
    @Value("classpath:fraction-invalid.csv")
    private Resource invalidFractionCsv;

    private CsvMapper<Fraction> mapper;

    @Before
    public void setUp() {
        this.mapper = new FractionCsvMapper();
    }

    @Test
    public void fromInputStreamValidInputTest() throws IOException {
        List<Fraction> fractions = mapper.fromInputStream(fractionCsv.getInputStream());
        assertThat(fractions)
                .hasSize(2);
    }

    @Test(expected = Exception.class)
    public void fromInputStreamInvalidInputTest() throws IOException {
        mapper.fromInputStream(invalidFractionCsv.getInputStream());
    }
}
