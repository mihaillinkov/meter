package com.mlinkov.meter.mapper;

import com.mlinkov.meter.model.MeterReading;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class MeterCsvMapperTests {
    @Value("classpath:meter.csv")
    private Resource meterCsv;
    @Value("classpath:meter-invalid.csv")
    private Resource invalidMeterCsv;

    private CsvMapper<MeterReading> mapper;

    @Before
    public void setUp() {
        this.mapper = new MeterCsvMapper();
    }

    @Test
    public void fromInputStreamValidInputTest() throws IOException {
        List<MeterReading> fractions = mapper.fromInputStream(meterCsv.getInputStream());
        assertThat(fractions)
                .hasSize(2);
    }

    @Test(expected = Exception.class)
    public void fromInputStreamInvalidInputTest() throws IOException {
        mapper.fromInputStream(invalidMeterCsv.getInputStream());
    }
}
