package com.mlinkov.meter.service;

import com.mlinkov.meter.exception.RequestValidationException;
import com.mlinkov.meter.model.Fraction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.mlinkov.meter.service.TestHelper.*;

@RunWith(SpringRunner.class)
public class FractionServiceTests {
    private final static double[] VALID_RATIOS = {0.2, 0.1, 0.1, 0.05, 0.04, 0.03, 0.07, 0.1, 0, 0.01, 0.1, 0.2};
    private final static double[] INVALID_RATIOS = {0.2, 0.1, 0.1, 0.001, 0, 0.1, 0, 0.1, 0, 0, 0.2, 0.2};
    private final static String PROFILE_A = "A";

    private FractionService service;

    @Before
    public void setUp() {
        this.service = new FractionService();
    }

    @Test
    public void validateBySumEqualOneTest() {
        Fraction fraction = new Fraction(PROFILE_A, initMap(VALID_RATIOS));
        service.validate(Arrays.asList(fraction));
    }

    @Test(expected = RequestValidationException.class)
    public void validateBySumNotEqualOneTest() {
        Fraction fraction = new Fraction(PROFILE_A, initMap(INVALID_RATIOS));
        service.validate(Arrays.asList(fraction));
    }
}
