package com.mlinkov.meter.service;

import com.mlinkov.meter.model.Month;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestHelper {
    public static Map<String, BigDecimal> initMap(double... values) {
        AtomicInteger i = new AtomicInteger(0);
        return Stream.of(Month.values())
                .collect(Collectors.toMap(Month::name, month -> BigDecimal.valueOf(values[i.getAndIncrement()])));
    }
}
