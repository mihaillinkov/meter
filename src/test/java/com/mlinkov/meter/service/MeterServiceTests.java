package com.mlinkov.meter.service;

import com.mlinkov.meter.exception.EntityNotFoundException;
import com.mlinkov.meter.model.Fraction;
import com.mlinkov.meter.model.MeterReading;
import com.mlinkov.meter.model.MeterReadingProcessingResult;
import com.mlinkov.meter.model.Month;
import com.mlinkov.meter.model.ProcessingStatus;
import com.mlinkov.meter.repository.FractionRepository;
import com.mlinkov.meter.repository.MeterRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.mlinkov.meter.service.TestHelper.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class MeterServiceTests {
    private final static String MERER_ID_1 = "0001";
    private final static String MERER_ID_2 = "0002";
    private final static String MERER_ID_3 = "0003";
    private final static String MERER_ID_4 = "0004";

    private final static String PROFILE_1 = "A";
    private final static String PROFILE_2 = "B";

    private final static double[] VALID_METER_VALUES = {20, 30, 40, 45, 50, 50, 50, 53, 60, 73, 83, 105};
    private final static double[] INVALID_METER_VALUES = {20, 3, 40, 45, 50, 50, 50, 53, 60, 73, 83, 105};
    private final static double[] INVALID_TOLERANCE_VALUES = {10, 20, 30, 30, 40, 50, 60, 80, 110, 110, 150, 170};

    private final static double[] VALID_RATIOS = {0.2, 0.1, 0.1, 0.05, 0.05, 0, 0, 0.03, 0.07, 0.1, 0.1, 0.2};

    @Mock
    private MeterRepository meterRepository;
    @Mock
    private FractionRepository fractionRepository;

    private MeterService service;

    @Before
    public void setUp() {
        service = new MeterService();
        service.setMeterRepository(meterRepository);
        service.setFractionRepository(fractionRepository);

        when(meterRepository.findById(eq(MERER_ID_1)))
                .thenReturn(Optional.of(new MeterReading(MERER_ID_1, PROFILE_1, initMap(VALID_METER_VALUES))));
        when(meterRepository.findById(eq(MERER_ID_2)))
                .thenReturn(Optional.ofNullable(null));
        when(fractionRepository.findByProfile(eq(PROFILE_1)))
                .thenReturn(Optional.of(new Fraction(PROFILE_1, initMap(VALID_RATIOS))));
        when(fractionRepository.findByProfile(eq(PROFILE_2)))
                .thenReturn(Optional.ofNullable(null));
    }

    @Test
    public void getMonthConsumptionTest() {
        assertThat(service.getMonthConsumption(MERER_ID_1, Month.JAN))
                .isEqualByComparingTo(BigDecimal.valueOf(20));
        assertThat(service.getMonthConsumption(MERER_ID_1, Month.DEC))
                .isEqualByComparingTo(BigDecimal.valueOf(22));
        assertThat(service.getMonthConsumption(MERER_ID_1, Month.APR))
                .isEqualByComparingTo(BigDecimal.valueOf(5));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getMonthConsumptionNotExistMeterTest() {
        service.getMonthConsumption(MERER_ID_2, Month.APR);
    }

    @Test
    public void validateAndGetErrorTest() {
        MeterReading validMeterReading = new MeterReading(MERER_ID_1, PROFILE_1, initMap(VALID_METER_VALUES));
        MeterReading notExistProfileMeterReading = new MeterReading(MERER_ID_2, PROFILE_2, initMap(VALID_METER_VALUES));
        MeterReading invalidValueMeterReading = new MeterReading(MERER_ID_3, PROFILE_1, initMap(INVALID_METER_VALUES));
        MeterReading invalidToleranceMeterReading = new MeterReading(MERER_ID_4, PROFILE_1, initMap(INVALID_TOLERANCE_VALUES));

        List<MeterReadingProcessingResult> result = service.save(Arrays.asList(
                validMeterReading, notExistProfileMeterReading, invalidValueMeterReading, invalidToleranceMeterReading
        ));

        assertThat(result.get(0).getStatus())
                .isEqualTo(ProcessingStatus.OK);
        assertThat(result.get(1).getMessage())
                .isEqualTo(String.format(MeterService.FRACTION_NOT_EXIST_MESSAGE, PROFILE_2));
        assertThat(result.get(2).getMessage())
                .isEqualTo(String.format(MeterService.METER_READING_VALUE_ERROR_MESSAGE, Month.FEB.name()));
        assertThat(result.get(3).getMessage())
                .isEqualTo(String.format(MeterService.METER_READING_TOLERANCE_ERROR_MESSAGE, Month.JAN.name()));
    }
}
